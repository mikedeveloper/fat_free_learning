<?php

class Controller
{
	// Establish a connection with the database
	public function __construct()
	{
		// Set the database connection here.
		$this->db=new DB\SQL(
		    'mysql:host=localhost;port=3306;dbname=scrabble',
		    'mike',
		    '******************'
		);
	}

	/**
	* We'll use here maybe later to test if a user is logged in later
	*/
	public function beforeRoute($f3)
	{
		// Something will go here.
	}

	// A simple funcgtion to check if the phone number is numeric and is no longer than 11 digits.
	public function checkPhoneNumber($f3)
	{
			// Remove any whitespace from the phone number.
			$f3->set('fields.contact_number', str_replace(' ', "",$f3->get('fields.contact_number')));
			// Check that the phone number now is no longer than 11 digits and is numeric - if not, stop there.
			if(strlen($f3->get('fields.contact_number'))>11 || !is_numeric($f3->get('fields.contact_number')))
			{
				// Set the add user form page as we're staying there.
				$f3->mset(
						array(
							'error.fields'=>'That phone number is incorrect!'
						)
					);
				// Nope, fail and return false to kill the script.
				return false;
			}

			// It's all ok, so let the addition/edit carry on.
			return true;
	}

		// A function to check we have all the required fields from the form. In this case, all of them! 
		public function submittedFieldCheck($f3)
		{
			// Create an array to hold the empty fields in.
			$this->fields = array();

			// Iterate the fields and find a missing one and if we do, hold it right there. 
			foreach($f3->get('POST') as $key=>$value)
			{
				// If a field is empty, add to the array and send back at the end.
				if(empty($value))
				{
					$this->fields[] = $key;
				}
			}

			// If all fields are fine an empty array is returned, otherwise the array has missing form fields themselves.
			return $this->fields;
		}


	// Get the template class initiated and render the index.php page on all routes.
	public function afterRoute($f3)
	{
		// We'll use this to instantiate the template instance and render it out using the fat free templating engine.
		echo \Template::instance()->render('index.php');
	}
}