<div class="row">
	<div class="col-md-12">
		<h4><span class="badge"> {{count(@result)}} </span> result(s).</h4>

		<p><a class="btn btn-primary" href="/git_repos/fat_free_learning/searchuser" role="button">Search again</a></p>

		<table class="table table-striped table-bordered table-hover">
			<tr>
				<th>First name</th>
				<th>Last name</th>
				<th>Date joined</th>
				<th>Contact number</th> 
			</tr>
			<repeat group="{{ @result }}" value="{{ @item }}">
				<tr>
					<td><span>{{ @item.first_name  }}</span></td>
					<td><span>{{ @item.last_name  }}</span></td>
					<td><span>{{ date('d/m/Y H:i\h\r\s',strtotime(@item.date_joined)) }}</span></td>
					<td><span>{{ @item.contact_number }}</span></td>
				</tr>
			</repeat>
		</table>
		<p class="small">Scroll the table horizontally</p>    	
	</div>
</div>