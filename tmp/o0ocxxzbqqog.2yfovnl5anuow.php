<div class="row">
	<div class="col-md-12">
		<h3>Please complete the form below to access the members form.</h3>
    </div>

    <div class="col-md-8">
        <?php if ($error['message']): ?>
            
            	<p style="padding:10px;" class="bg-primary"><?php echo $error['message']; ?></p>
            
        <?php endif; ?>
        <?php if ($results['message']): ?>
            
            	<p style="padding:10px;" class="bg-primary"><?php echo $results['message']; ?></p>
        	
        <?php endif; ?>

        <form method="POST">
          <div class="form-group">
            <label for="name">Surname</label>
            <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $name; ?>" placeholder="Enter the surname of the member"/>
            <p><span class="required">Required field</span></p>
          </div>
        	<button type="submit" class="btn btn-primary">Search</button>
        </form>
	</div>
</div>
